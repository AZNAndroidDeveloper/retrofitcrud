package uz.azn.retrofit2.intro

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import uz.azn.retrofit2.R
import uz.azn.retrofit2.create.CreateUserFragment
import uz.azn.retrofit2.databinding.FragmentIntroBinding
import uz.azn.retrofit2.intro.adapter.IntroAdapter
import uz.azn.retrofit2.intro.viewModel.IntroViewModel
import uz.azn.retrofit2.model.User


class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var binding: FragmentIntroBinding
    val bundle = Bundle()
    val createUserFragment = CreateUserFragment()

    private val introAdapter = IntroAdapter(
        onItemClicked = {
            onItemClicked(it)
        }
    )
    private lateinit var introViewModel: IntroViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        introViewModel = ViewModelProvider(requireActivity()).get(IntroViewModel::class.java)
        initializeRv()
        initializeViewModel()
        binding.btnSearch.setOnClickListener {
            searchUsers()
        }
        binding.btnCreateUser.setOnClickListener {
            fragmentManager!!.beginTransaction().replace(R.id.frame_layout, createUserFragment)
                .commit()
        }
    }

    private fun initializeRv() {
        val decoration = DividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL)
        with(binding) {
            recyclerView.apply {
                layoutManager = LinearLayoutManager(requireContext())
                addItemDecoration(decoration)
                adapter = introAdapter
            }
        }
    }

    private fun initializeViewModel() {
        introViewModel.getUsersObservable().observe(requireActivity(), Observer {
            if (it == null) {
                Toast.makeText(requireContext(), "Data not found", Toast.LENGTH_SHORT).show()
            } else {
                introAdapter.setData(it.data)
                introAdapter.notifyDataSetChanged()
            }
        })
        introViewModel.getUsers()

    }

    private fun searchUsers() {
        if (binding.etSearch.text.isNotEmpty()) {
            introViewModel.searchUsers(binding.etSearch.text.toString())
        } else
            introViewModel.getUsers()
    }

    private fun onItemClicked(user: User) {
        bundle.putString("user_id", user.id)
        createUserFragment.arguments = bundle
        fragmentManager!!.beginTransaction().replace(R.id.frame_layout, createUserFragment).commit()

    }


}