package uz.azn.retrofit2.intro.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.azn.retrofit2.model.User
import uz.azn.retrofit2.databinding.UserViewHolderBinding as UserBinding

class IntroAdapter( val onItemClicked:(user:User)->Unit) : RecyclerView.Adapter<IntroAdapter.UserViewHolder>() {

    private val elements = mutableListOf<User>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            UserBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = elements.size

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.onBind(elements[position])
    }

    inner class UserViewHolder(private val binding: UserBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(user: User) {
            with(binding) {
                tvGender.text = user.gender
                tvEmail.text = user.email
                tvName.text = user.name

               constraint.setOnClickListener {
                    onItemClicked.invoke(user)
                }
            }
        }
    }

    fun setData(userElement: List<User>) {
        elements.apply { clear(); addAll(userElement) }
        notifyDataSetChanged()
    }
}