package uz.azn.retrofit2.intro.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.azn.retrofit2.api.ApiService
import uz.azn.retrofit2.api.RetrofitInstance
import uz.azn.retrofit2.model.UserList

class IntroViewModel : ViewModel() {
    private var recycleList: MutableLiveData<UserList> = MutableLiveData()
    private val retorfit = RetrofitInstance.getRetrofitInstance().create(ApiService::class.java)

    fun getUsersObservable(): LiveData<UserList> {
        return recycleList
    }

    fun getUsers() {
        val call = retorfit.getAllUsers()
        call.enqueue(object : Callback<UserList> {
            override fun onFailure(call: Call<UserList>, t: Throwable) {
                recycleList.postValue(null)
            }

            override fun onResponse(call: Call<UserList>, response: Response<UserList>) {
                if (response.isSuccessful) {
                    recycleList.postValue(response.body())
                }
            }

        })
    }
    fun searchUsers(searchUsers:String){
        val call = retorfit.searchUsers(searchUsers)
        call.enqueue(object : Callback<UserList> {
            override fun onFailure(call: Call<UserList>, t: Throwable) {
                recycleList.postValue(null)
            }

            override fun onResponse(call: Call<UserList>, response: Response<UserList>) {
                if (response.isSuccessful) {
                    recycleList.postValue(response.body())
                }
            }

        })
    }
}