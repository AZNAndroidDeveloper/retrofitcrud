package uz.azn.retrofit2.api

import retrofit2.Call
import retrofit2.http.*
import uz.azn.retrofit2.constans.ConstantsApiPath
import uz.azn.retrofit2.model.User
import uz.azn.retrofit2.model.UserList
import uz.azn.retrofit2.model.UserResponse

interface ApiService {

    //Get all users
    @GET("users")
    @Headers(ConstantsApiPath.HEADRSACCEPT, ConstantsApiPath.HEADRSContent)
    fun getAllUsers(): Call<UserList>

    // search user
    @GET("users")
    @Headers(
        ConstantsApiPath.HEADRSACCEPT,
        ConstantsApiPath.HEADRSContent
    )
    fun searchUsers(@Query("name") searchQuery: String): Call<UserList>

    // Get user by id
    /**
     * izoh Path qoyishimizdan sabab shuki  biz bitta raqam berapmiz
     * */
    @GET("users/{user_id}")
    @Headers(
        ConstantsApiPath.HEADRSACCEPT,
        ConstantsApiPath.HEADRSContent
    )
    fun getUser(@Path("user_id") user_id: String): Call<UserResponse>

    //Creating new user
    @POST("users")
    @Headers(
        ConstantsApiPath.HEADRSACCEPT,
        ConstantsApiPath.HEADRSContent,
        "Authorization: Bearer ${ConstantsApiPath.API_KEY}"
    )
    fun createUser(@Body params: User): Call<UserResponse>

    // updating a user
    /**
     * Biz body qoshishimizdan maqsad  id dage user objectni oladi
     * */
    @PATCH("users/{user_id}")
    @Headers(
        ConstantsApiPath.HEADRSACCEPT,
        ConstantsApiPath.HEADRSContent,
        "Authorization: Bearer ${ConstantsApiPath.API_KEY}"
    )
    fun updateUser(@Path("user_id") user_id: String, @Body params: User): Call<UserResponse>

    // Deleting user
    @DELETE("users/{user_id}")
    @Headers(
        ConstantsApiPath.HEADRSACCEPT,
        ConstantsApiPath.HEADRSContent,
        "Authorization: Bearer ${ConstantsApiPath.API_KEY}"
    )
    fun deleteUser(@Path("user_id") user_id: String): Call<UserResponse>
}