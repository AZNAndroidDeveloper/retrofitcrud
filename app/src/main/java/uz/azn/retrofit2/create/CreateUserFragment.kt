package uz.azn.retrofit2.create

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import uz.azn.retrofit2.R
import uz.azn.retrofit2.create.viewmodel.CreateUserViewModel
import uz.azn.retrofit2.databinding.FragmentCreateUserBinding
import uz.azn.retrofit2.intro.IntroFragment
import uz.azn.retrofit2.model.User


class CreateUserFragment : Fragment(R.layout.fragment_create_user) {
    private lateinit var binding: FragmentCreateUserBinding
    private lateinit var viewModel: CreateUserViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentCreateUserBinding.bind(view)
        viewModel = ViewModelProvider(requireActivity()).get(CreateUserViewModel::class.java)
//        initializeViewModel()
        createUserObserver()
        val user_id = arguments?.getString("user_id", "")
        if (user_id != null) {
            loadUserData(user_id)
        }
        Log.d("TAG", "onViewCreated: $user_id")
        binding.btnCreate.setOnClickListener {
            createUser(user_id)
        }
        binding.btnDelete.setOnClickListener {
deleteUser(user_id!!)
        }
    }
    private fun deleteUser(user_id:String){
        viewModel.getDeleteObservable().observe(requireActivity(), Observer {
            if (it ==null){
                Toast.makeText(requireContext(), "Failed to delete", Toast.LENGTH_SHORT).show()

            }
            else{
                Toast.makeText(requireContext(), "Successfully delete", Toast.LENGTH_SHORT).show()
                fragmentManager!!.beginTransaction().replace(R.id.frame_layout, IntroFragment()).commit()

            }
        })
        viewModel.deleteUser(user_id)
    }

    private fun loadUserData(user_id: String?) {
        viewModel.getLoadObservable().observe(requireActivity(), Observer {
            if (it != null) {
                with(binding) {
                    etName.setText(it.data?.name)
                    etEmail.setText(it.data?.email)
                    etGender.setText(it.data?.gender)
                    btnCreate.text = "Update"
                    btnDelete.visibility = View.VISIBLE
                }
            }
        })
        viewModel.getUser(user_id!!)
    }

    private fun createUser(user_id: String?) {
        with(binding) {
            val email = etEmail.text.toString()
            val gender = etGender.text.toString()
            val name = etName.text.toString()
            val user = User("", name, email, "Active", gender)
            if (user_id == null) {
                viewModel.createUser(user)
            } else {
                viewModel.updateUser(user_id,user)
            }
        }
    }

    private fun createUserObserver() {
        viewModel.getCreateUserObservable().observe(requireActivity(), Observer {
            if (it == null) {
                Toast.makeText(requireContext(), "Faild...${it}", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(requireContext(), "UserCreated $it", Toast.LENGTH_SHORT).show()
                fragmentManager!!.beginTransaction().replace(R.id.frame_layout, IntroFragment()).commit()

            }
        })
    }

}