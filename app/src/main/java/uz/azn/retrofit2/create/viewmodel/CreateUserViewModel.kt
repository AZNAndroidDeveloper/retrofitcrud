package uz.azn.retrofit2.create.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.azn.retrofit2.api.ApiService
import uz.azn.retrofit2.api.RetrofitInstance
import uz.azn.retrofit2.model.User
import uz.azn.retrofit2.model.UserList
import uz.azn.retrofit2.model.UserResponse

class CreateUserViewModel : ViewModel() {
    private val createUserLiveData: MutableLiveData<UserResponse> = MutableLiveData()
    private val loadUserData: MutableLiveData<UserResponse?> = MutableLiveData()
    private val deleteUserData:MutableLiveData<UserResponse?> = MutableLiveData()
    private val retorfit = RetrofitInstance.getRetrofitInstance().create(ApiService::class.java)


    fun getCreateUserObservable(): LiveData<UserResponse> = createUserLiveData
    fun createUser(user: User) {
        val call = retorfit.createUser(user)
        call.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                createUserLiveData.postValue(null)
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                if (response.isSuccessful) {
                    createUserLiveData.postValue(response.body())
                }
            }

        })
    }
    fun updateUser(user_id:String,user: User) {
        val call = retorfit.updateUser(user_id,user)
        call.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
               createUserLiveData.postValue(null)
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                if (response.isSuccessful) {
                   createUserLiveData.postValue(response.body())
                }
            }

        })
    }
    fun deleteUser(user_id:String) {
        val call = retorfit.deleteUser(user_id)
        call.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                deleteUserData.postValue(null)
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                if (response.isSuccessful) {
                    deleteUserData.postValue(response.body())
                }
            }

        })
    }

    fun getLoadObservable(): LiveData<UserResponse?> {
        return loadUserData
    }

    fun getDeleteObservable(): LiveData<UserResponse?> {
        return deleteUserData
    }

    fun getUser(user_id: String) {
        val call = retorfit.getUser(user_id!!)
        call.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                loadUserData.postValue(null)
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                if (response.isSuccessful) {
                    loadUserData.postValue(response.body())
                }
            }

        })
    }

}
