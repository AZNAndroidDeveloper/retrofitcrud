package uz.azn.retrofit2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import uz.azn.retrofit2.intro.IntroFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(R.id.frame_layout,
            IntroFragment()
        )
            .commit()
    }
}